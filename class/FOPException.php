<?php


	/**
	 *
	 *   FlaskPHP-FOP
	 *   ------------
	 *   The FOP exception
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\FOP;
	use Codelab\FlaskPHP;


	class FOPException extends FlaskPHP\Exception\Exception
	{
	}


?>