<?php


	/**
	 *
	 *   FlaskPHP-FOP
	 *   ------------
	 *   The FOP wrapper class
	 *
	 *   @author   Codelab Solutions OÜ <codelab@codelab.ee>
	 *   @license  https://www.flaskphp.com/LICENSE MIT
	 *
	 */


	namespace Codelab\FlaskPHP\FOP;
	use Codelab\FlaskPHP\Template\Template;


	class FOP
	{


		/**
		 *   FOP system path
		 *   @var string
		 *   @access public
		 */

		public $fopPath = null;


		/**
		 *
		 *   The constructor
		 *   ---------------
		 *   @access public
		 *   @return FOP
		 *
		 */

		public function __construct()
		{
			// Determine FOP system path
			if (Flask()->Config->get('fop.path'))
			{
				$this->fopPath=Flask()->Config->get('fop.path');
			}
			else
			{
				$this->fopPath=realpath(__DIR__.'/../fop');
			}

			// Init FOP instance
			$this->initFOP();
		}


		/**
		 *
		 *   Init the FOP instance
		 *   ---------------------
		 *   @access public
		 *   @throws \Exception
		 *   @return void
		 *
		 */

		public function initFOP()
		{
			// This can be extended in the subclass if necessary.
		}


		/**
		 *
		 *   Get XCONF
		 *   ---------
		 *   @access public
		 *   @param string $basePath Base path
		 *   @return string|null
		 *
		 */

		public function getXConf( string $basePath ): ?string
		{
			$xconf='<?xml version="1.0"?>
				<fop version="1.0">
					<base>'.$basePath.'</base>
					<source-resolution>72</source-resolution>
					<target-resolution>72</target-resolution>
					<default-page-settings height="29.7cm" width="21cm"/>
					<renderers>
						<renderer mime="application/pdf">
							<filterList>
								<value>flate</value>
							</filterList>
							<fonts>
								<directory recursive="true">'.$this->fopPath.'</directory>
								<directory recursive="true">'.$basePath.'</directory>
							</fonts>
						</renderer>
						<renderer mime="application/vnd.hp-PCL">
						</renderer>
						<renderer mime="image/svg+xml">
							<format type="paginated"/>
							<link value="true"/>
							<strokeText value="false"/>
						</renderer>
						<renderer mime="image/svg">
							<format type="paginated"/>
							<link value="true"/>
							<strokeText value="false"/>
						</renderer>
						<renderer mime="application/awt">
						</renderer>
						<renderer mime="image/png">
						</renderer>
						<renderer mime="image/tiff">
						</renderer>
						<renderer mime="text/xml">
						</renderer>
						<renderer mime="text/plain">
							<pageSize columns="80"/>
						</renderer>
					</renderers>
				</fop>
			';
			return $xconf;
		}


		/**
		 *
		 *   Get FOP cmd
		 *   -----------
		 *   @access public
		 *   @param string $outputFormat Output format (pdf,rtf)
		 *   @param string $basePath Base path
		 *   @param string $xmlFile Path to XML file
		 *   @param string $xslFile Path to XSL file
		 *   @param string|null $xConfFile Path to XCONF file (or null, if it was not generated)
		 *   @param string $outputFile Path to output file
		 *   @return string
		 *
		 */

		public function getFOPCmd( string $outputFormat, string $basePath, string $xmlFile, string $xslFile, ?string $xConfFile, string $outputFile ): string
		{
			$fopCmd=$this->fopPath.'/fop'
				.' -xml '.$xmlFile
				.' -xsl '.$xslFile
				.' -'.$outputFormat.' '.$outputFile
				.' -c '.$xConfFile;
			return $fopCmd;
		}


		/**
		 *
		 *   Generate PDF from XML+XSL
		 *   -------------------------
		 *   @access public
		 *   @var string $XML XML (as string)
		 *   @var string $XSL Path to XSL file
		 *   @var string Base path (path or null = autodetect from XSL)
		 *   @var string $outputFile Output PDF as file (false = no, filename = yes)
		 *   @var bool $returnPDF Return PDF content
		 *   @throws FOPException
		 *   @return string
		 *
		 */

		public function generatePDF( string $XML, string $XSL, string $basePath=null, string $outputFile=null, bool $returnPDF=true )
		{
			// Check XSL
			if (!stream_resolve_include_path($XSL)) throw new FOPException('XSL file not readable');

			// Determine base path if needed
			if ($basePath===null)
			{
				$pathInfo=pathinfo(stream_resolve_include_path($XSL));
				$basePath=$pathInfo['dirname'];
			}

			// Localize strings in the XML
			$XML=Template::parseContent($XML,true);

			// Base filename
			$fnBase=Flask()->Config->getTmpPath().'/'.md5(uniqid()).'.'.time();

			// Write XML file
			file_put_contents($fnBase.'.xml',$XML);

			// Configuration
			$xconf=$this->getXConf($basePath);
			if ($xconf!==null) file_put_contents($fnBase.'.xconf',$xconf);

			// Generate FOP command-line
			$fopCWD=$basePath;
			$fopCmd=$this->getFOPCmd(
				'pdf',
				$basePath,
				$fnBase.'.xml',
				$XSL,
				$fnBase.'.xconf',
				(!empty($outputFile)?$outputFile:$fnBase.'.pdf')
			);

			// Run FOP
			$errors='';
			exec_with_cwd($fopCmd,$fopCWD,$errors);

			//  Check output
			if (!file_exists(!empty($outputFile)?$outputFile:$fnBase.'.pdf'))
			{
				unlink($fnBase.'.xml');
				unlink($fnBase.'.xconf');
				if (Flask()->Debug->devEnvironment)
				{
					$debugFile=$fnBase.'.foperror';
					$debugContent="Path: ".$fopCWD."\n";
					$debugContent.="Cmd: ".$fopCmd."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="Output:\n\n";
					$debugContent.=$errors."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="XML:\n\n";
					$debugContent.=$XML."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="XSL:\n\n";
					$debugContent.=file_get_contents($XSL)."\n";
					file_put_contents($debugFile,$debugContent);
					throw new FOPException('Error generating PDF. Details in: '.$debugFile);
				}
				else
				{
					throw new FOPException('Error generating PDF.');
				}
			}
			if ($returnPDF) $PDF=file_get_contents(!empty($outputFile)?$outputFile:$fnBase.'.pdf');

			// Clean up
			unlink($fnBase.'.xml');
			if ($xconf!==null) unlink($fnBase.'.xconf');
			if (empty($outputFile)) unlink($fnBase.'.pdf');

			// iHaz Success
			if ($returnPDF) return $PDF;
			return '';
		}


		/**
		 *
		 *   Generate RTF from XML+XSL
		 *   -------------------------
		 *   @access public
		 *   @var string $XML XML (as string)
		 *   @var string $XSL Path to XSL file
		 *   @var string Base path (path or null = autodetect from XSL)
		 *   @var string $outputFile Output PDF as file (false = no, filename = yes)
		 *   @var bool $returnRTF Return RTF content
		 *   @throws FOPException
		 *   @return string
		 *
		 */

		public function generateRTF( string $XML, string $XSL, string $basePath=null, string $outputFile=null, bool $returnRTF=true )
		{
			// Check XSL
			if (!stream_resolve_include_path($XSL)) throw new FOPException('XSL file not readable');

			// Determine base path if needed
			if ($basePath===null)
			{
				$pathInfo=pathinfo(stream_resolve_include_path($XSL));
				$basePath=$pathInfo['dirname'];
			}

			// Localize strings in the XML
			$XML=Template::parseContent($XML,true);

			// Base filename
			$fnBase=Flask()->Config->getTmpPath().'/'.md5(uniqid()).'.'.time();

			// Write XML file
			file_put_contents($fnBase.'.xml',$XML);

			// Configuration
			$xconf=$this->getXConf($basePath);
			if ($xconf!==null) file_put_contents($fnBase.'.xconf',$xconf);

			// Generate FOP command-line
			$fopCWD=$basePath;
			$fopCmd=$this->getFOPCmd(
				'rtf',
				$basePath,
				$fnBase.'.xml',
				$XSL,
				$fnBase.'.xconf',
				(!empty($outputFile)?$outputFile:$fnBase.'.rtf')
			);

			// Run FOP
			$errors='';
			exec_with_cwd($fopCmd,$fopCWD,$errors);

			//  Check output
			if (!file_exists(!empty($outputFile)?$outputFile:$fnBase.'.rtf'))
			{
				unlink($fnBase.'.xml');
				unlink($fnBase.'.xconf');
				if (Flask()->Debug->devEnvironment)
				{
					$debugFile=$fnBase.'.foperror';
					$debugContent="Path: ".$fopCWD."\n";
					$debugContent.="Cmd: ".$fopCmd."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="Output:\n\n";
					$debugContent.=$errors."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="XML:\n\n";
					$debugContent.=$XML."\n\n";
					$debugContent.="--- 8< ---\n\n";
					$debugContent.="XSL:\n\n";
					$debugContent.=file_get_contents($XSL)."\n";
					file_put_contents($debugFile,$debugContent);
					throw new FOPException('Error generating RTF. Details in: '.$debugFile);
				}
				else
				{
					throw new FOPException('Error generating RTF.');
				}
			}
			$RTF=file_get_contents(!empty($outputFile)?$outputFile:$fnBase.'.rtf');

			// Clean up
			unlink($fnBase.'.xml');
			if ($xconf!==null) unlink($fnBase.'.xconf');
			if (empty($outputFile)) unlink($fnBase.'.rtf');

			// iHaz Success
			if ($returnRTF) return $RTF;
			return '';
		}


	}


?>